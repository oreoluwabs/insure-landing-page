import { BrowserRouter } from "react-router-dom";
import { MyTheme } from "./context";
import { HomePage } from "./views";

function App() {
  return (
    <>
      <BrowserRouter>
        <MyTheme>
          <HomePage />
        </MyTheme>
      </BrowserRouter>
    </>
  );
}

export default App;
