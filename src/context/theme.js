import React from "react";
import { ThemeProvider } from "styled-components";
import { GlobalStyle } from "../components/global";

const theme = {
  colors: {
    primary: {
      main: "hsl(256, 26%, 20%)",
      accent: "hsl(216, 30%, 68%)",
    },
    neutral: {
      "very-dark-violet": "hsl(270, 9%, 17%)",
      "dark-grayish-violet": "hsl(273, 4%, 51%)",
      "very-light-gray": "hsl(0, 0%, 98%)",
    },

    "dark-violet": "hsl(256, 26%, 20%)",
    "grayish-blue": "hsl(216, 30%, 68%)",
    "very-dark-violet": "hsl(270, 9%, 17%)",
    "dark-grayish-violet": "hsl(273, 4%, 51%)",
    "very-light-gray": "hsl(0, 0%, 98%)",
  },

  typography: {
    headings: {
      "font-family": "'DM Serif Display', serif",
    },
    body: {
      "font-family": "'Karla', sans-serif",
      "font-size": "16px",
    },
  },

  breakpoints: {
    width: {
      mobile: "375px",
      desktop: "1440px",
    },
  },
};

const MyTheme = ({ children }) => {
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      {children}
    </ThemeProvider>
  );
};

export default MyTheme;
