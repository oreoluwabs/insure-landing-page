import React from "react";
import BannerComponent from "../components/Banner";
import Footer from "../components/Footer";
import { HeroComponent, InfoSectionComponent } from "../components/Home";
import Navbar from "../components/Navbar";

const HomePage = () => {
  return (
    <div>
      <Navbar />
      <HeroComponent />
      <InfoSectionComponent />
      <BannerComponent />
      <Footer />
    </div>
  );
};

export default HomePage;
