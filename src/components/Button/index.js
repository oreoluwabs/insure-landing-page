import styled, { css } from "styled-components";
import { Link as LinkRRouter } from "react-router-dom";

const lightGray = ({ theme }) => theme.colors.neutral["very-light-gray"];

const sharedStyles = css`
  background-color: transparent;
  padding: 10px 25px;
  border: 1px solid ${lightGray};
  color: ${lightGray};
  text-decoration: none;
  text-transform: uppercase;
  transition: all 0.5s;

  &:hover {
    background-color: ${lightGray};
    color: ${({ theme }) => theme.colors.neutral["very-dark-violet"]};
  }
`;

export const Button = styled.button`
  ${sharedStyles}
`;

export const ButtonLink = styled(LinkRRouter)`
  ${sharedStyles}
`;
