import styled from "styled-components";

const color = ({ theme, color }) =>
  color || theme.colors.neutral["very-light-gray"];

export const SectionIndicator = styled.hr`
  // background-color: ${color};
  // height: 1px;
  border: none;
  border-top: 1px solid ${color};
  width: 120px;
  margin-bottom: 40px;

  @media screen and (max-width: 870px) {
    margin-left: auto;
    margin-right: auto;
  }
`;
