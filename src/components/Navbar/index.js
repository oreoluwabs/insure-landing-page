import React, { useState } from "react";
import logo from "../../assets/images/logo.svg";
import MenuIcon from "../../assets/images/icon-hamburger.svg";
import CloseIcon from "../../assets/images/icon-close.svg";
import { Container } from "../Layout";
import {
  Drawer,
  DrawerWrapper,
  Header,
  MobileIcon,
  Nav,
  NavItem,
  NavLink,
  NavLinkButton,
  NavLinks,
  NavLogo,
} from "./NavbarElements";

const NavLinkMenu = () => {
  return (
    <>
      <NavItem>
        <NavLink to="/">How we work</NavLink>
      </NavItem>
      <NavItem>
        <NavLink to="/">Blog</NavLink>
      </NavItem>
      <NavItem>
        <NavLink to="/">Account</NavLink>
      </NavItem>
      <NavItem>
        <NavLinkButton to="/">View plans</NavLinkButton>
      </NavItem>
    </>
  );
};

const Navbar = () => {
  const [drawerState, setDrawerState] = useState(false);
  const toggleSidebar = () => setDrawerState(!drawerState);
  return (
    <>
      <Header isOpen={drawerState}>
        <Container>
          <Nav>
            <NavLogo to="/">
              <img src={logo} alt="insure logo" />
            </NavLogo>

            <MobileIcon onClick={toggleSidebar}>
              {!drawerState && <img src={MenuIcon} alt="menu button" />}
              {drawerState && <img src={CloseIcon} alt="close button" />}
            </MobileIcon>

            <NavLinks>
              <NavLinkMenu />
            </NavLinks>

            <Drawer isOpen={drawerState}>
              <DrawerWrapper>
                <NavLinkMenu />
              </DrawerWrapper>
            </Drawer>
          </Nav>
        </Container>
      </Header>
    </>
  );
};

export default Navbar;
