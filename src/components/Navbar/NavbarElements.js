import styled from "styled-components";
import { NavLink as NavLinkRRouter } from "react-router-dom";

const veryDarkViolet = ({ theme }) => theme.colors.neutral["very-dark-violet"];

const getImage = (imageName) =>
  require(`../../assets/images/${imageName}`).default;

export const Header = styled.header`
  position: ${({ isOpen }) => (isOpen ? "fixed" : "static")};
  top: ${({ isOpen }) => (isOpen ? "0" : "-100%")};
  width: 100%;
  background-color: white;
  z-index: 999;
`;

export const Nav = styled.nav`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 80px;
`;

export const NavLogo = styled(NavLinkRRouter)``;

export const MobileIcon = styled.div`
  display: none;

  @media screen and (max-width: 870px) {
    display: block;
    cursor: pointer;
  }
`;

export const NavLinks = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  @media screen and (max-width: 870px) {
    display: none;
  }
`;

export const NavItem = styled.div`
  padding: 0 15px;
  text-transform: uppercase;
  font-size: 12px;
  font-weight: 700;
  letter-spacing: 1.2px;

  &:last-of-type {
    padding-right: 0px;
  }

  @media screen and (max-width: 870px) {
    padding: 15px;
    font-size: 16px;
  }
`;

export const NavLink = styled(NavLinkRRouter)`
  text-decoration: none;
  color: ${({ theme }) => theme.colors.neutral["dark-grayish-violet"]};
  transition: all 0.2s;

  &:hover {
    color: ${veryDarkViolet};
  }

  @media screen and (max-width: 870px) {
    color: white;
  }
`;

export const NavLinkButton = styled(NavLinkRRouter)`
  padding: 10px 25px;
  border: 1px solid ${veryDarkViolet};
  color: ${veryDarkViolet};
  text-decoration: none;
  transition: all 0.5s;

  &:hover {
    background-color: ${veryDarkViolet};
    color: #fff;
  }

  @media screen and (max-width: 870px) {
    display: block;
    border: 1px solid white;
    color: white;
  }
`;

// .button {
//   display: block;
// }

export const Drawer = styled.aside`
position: fixed;
z-index: 999;
width: 100%;
height 100%;
background: ${veryDarkViolet};
top: 80px;
left: 0;
transition: 0.3s ease-in-out;
opacity: ${({ isOpen }) => (isOpen ? "100%" : "0")};
top: ${({ isOpen }) => (isOpen ? "80px" : "-100%")};
background-image: url(${getImage("bg-pattern-mobile-nav.svg")});
background-repeat: no-repeat;
background-position: bottom 14% center;
background-size: 100%;
`;

export const DrawerWrapper = styled.div`
  color: white;
  display: flex;
  flex-direction: column;
  justify-content: center;
  text-align: center;
  padding: 30px 20px;
`;
