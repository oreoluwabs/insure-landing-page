import styled from "styled-components";

// const mobileBreakPoint = ({ theme }) => theme.breakpoints.width.mobile;

const getImage = (imageName) =>
  require(`../../../assets/images/${imageName}`).default;

export const HeroSection = styled.section`
  background-color: ${({ theme }) => theme.colors.primary.main};
  max-height: 520px;
  position: relative;

  @media screen and (max-width: 1300px) {
    max-height: 550px;
  }

  @media screen and (max-width: 1000px) {
    max-height: 100%;
  }
`;

export const HeroBgPattern = styled.div`
  position: absolute;
  height: 100vh;
  width: 100%;
  background-image: url(${getImage("bg-pattern-intro-left-desktop.svg")}),
    url(${getImage("bg-pattern-intro-right-desktop.svg")});
  background-position: left top 80%, top -25% right;
  background-repeat: no-repeat;
  background-size: 10%, 23%;

  @media screen and (min-width: 2000px) {
    max-height: 1200px;
    background-position: left top 65%, top -20% right;
  }

  @media screen and (max-width: 1200px) {
    height: 900px;
    background-position: left bottom 0%, top 0% right;
    background-size: 15%, 30%;
  }

  @media screen and (max-width: 870px) {
    background-image: url(${getImage("bg-pattern-intro-left-mobile.svg")}),
      url(${getImage("bg-pattern-intro-right-mobile.svg")});
    height: 670px;
    background-position: left top, bottom right;
    background-size: auto;
  }
`;

export const HeroMain = styled.div`
  padding: 80px 0 90px;
  display: flex;
  justify-content: space-between;

  @media screen and (max-width: 870px) {
    flex-direction: column;
  }
`;

export const HeroCaption = styled.div`
  z-index: 999;
  color: #fff;
  flex: 0 0 40%;

  @media screen and (max-width: 870px) {
    z-index: 9;
    text-align: center;
    flex: 1;

    #hero-divider {
      display: none;
    }
  }
`;

export const HeroHeader = styled.h1`
  font-size: 3.5rem;
  font-weight: 700;
  letter-spacing: 2px;

  @media screen and (max-width: 870px) {
    font-size: 3rem;
  }

  @media screen and (max-width: 400px) {
    font-size: 2.5rem;
  }
`;

export const HeroParagraph = styled.p`
  margin: 1rem 0 1.5rem;
`;

export const HeroButtonWrapper = styled.div`
  display: flex;
  align-items: center;
  font-size: 12px;

  @media screen and (max-width: 870px) {
    justify-content: center;
  }
`;

export const HeroImageWrapper = styled.div`
  flex: 0 0 50%;
  height: 600px;
  width: 100%;
  background-image: url(${getImage("image-intro-desktop.jpg")});
  background-position: top right;
  background-repeat: no-repeat;
  background-size: contain;

  @media screen and (max-width: 1000px) {
    height: auto;
  }

  @media screen and (max-width: 870px) {
    display none;
  }
`;

//   @media screen and (max-width: ${mobileBreakPoint}) {
//     background-image: url(${getImage("image-intro-mobile.jpg")});
//   } ;

export const HeroImage = styled.img`
  width: 100%;
  object-fit: cover;
  display: none;

  @media screen and (max-width: 870px) {
    display: block;
  }
`;
