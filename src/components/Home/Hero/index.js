import React from "react";
import { ButtonLink } from "../../Button";
import { SectionIndicator } from "../../Divider";
import { Container } from "../../Layout";
import {
  HeroBgPattern,
  HeroButtonWrapper,
  HeroCaption,
  HeroHeader,
  HeroImage,
  HeroImageWrapper,
  HeroMain,
  HeroParagraph,
  HeroSection,
} from "./HeroElements";

const Hero = () => {
  return (
    <>
      <HeroSection>
        <HeroImage
          srcSet={`
                ${
                  require("../../../assets/images/image-intro-mobile.jpg")
                    .default
                } 300w
                       `}
          src={require("../../../assets/images/image-intro-mobile.jpg").default}
          alt="A happily insured family"
        />
        <HeroBgPattern />
        <Container>
          <HeroMain>
            <HeroCaption>
              <SectionIndicator id="hero-divider" />
              <HeroHeader>Humanizing your insurance.</HeroHeader>
              <HeroParagraph>
                Get your life insurance coverage easier and faster. We blend our
                expertise and technology to help you find the plan that's right
                for you. Ensure you and your loved ones are protected.
              </HeroParagraph>
              <HeroButtonWrapper>
                <ButtonLink to="">View plans</ButtonLink>
              </HeroButtonWrapper>
            </HeroCaption>
            <HeroImageWrapper />
          </HeroMain>
        </Container>
      </HeroSection>
    </>
  );
};

export default Hero;
