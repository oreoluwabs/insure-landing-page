import styled from "styled-components";

const darkGrayishViolet = ({ theme }) =>
  theme.colors.neutral["dark-grayish-violet"];

export const InfoSectionContainer = styled.section`
  margin: 300px 0 100px;

  @media screen and (max-width: 870px) {
    margin-top: 150px;
    text-align: center;
  }
`;

export const InfoFeatures = styled.div`
  margin: 70px 0 0;
  display: flex;
  justify-content: space-between;

  @media screen and (max-width: 870px) {
    flex-direction: column;
    // text-align: center;
  }
`;

export const InfoFeature = styled.div`
  &:not(:last-of-type) {
    margin-right: 20px;
  }
  //   width: 50%;

  @media screen and (max-width: 870px) {
    margin: 20px 0;
  }
`;

export const InfoHeader = styled.h3`
  margin: 20px 0;
`;

export const InfoText = styled.p`
  margin: 10px 0;
  color: ${darkGrayishViolet};
`;
