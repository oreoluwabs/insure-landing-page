import React from "react";
import { useTheme } from "styled-components";
import { SectionIndicator } from "../../Divider";
import { Container } from "../../Layout";
import { HeroHeader } from "../Hero/HeroElements";
import IconSnappyProcess from "../../../assets/images/icon-snappy-process.svg";
import IconAffordablePrices from "../../../assets/images/icon-affordable-prices.svg";
import IconPeopleFirst from "../../../assets/images/icon-people-first.svg";
import {
  InfoFeature,
  InfoFeatures,
  InfoHeader,
  InfoSectionContainer,
  InfoText,
} from "./InfoSectionElements";

const InfoSection = () => {
  const { colors } = useTheme();
  return (
    <InfoSectionContainer>
      <Container>
        <SectionIndicator color={colors.primary.main} />
        <HeroHeader>We're different</HeroHeader>
        <InfoFeatures>
          <InfoFeature>
            <img src={IconSnappyProcess} alt="quick and fast" />
            <InfoHeader>Snappy Process</InfoHeader>
            <InfoText>
              Our application process can be completed in minutes, not hours.
              Don't get stuck filling in tedious forms.
            </InfoText>
          </InfoFeature>
          <InfoFeature>
            <img src={IconAffordablePrices} alt="affordable prices" />
            <InfoHeader>Affordable Prices</InfoHeader>
            <InfoText>
              We don't want you worrying about high monthly costs. Our prices
              may be low, but we still offer the best coverage possible.
            </InfoText>
          </InfoFeature>
          <InfoFeature>
            <img src={IconPeopleFirst} alt="people oriented" />
            <InfoHeader>People First</InfoHeader>
            <InfoText>
              Our plans aren't full of conditions and clauses to prevent
              payouts. We make sure you're covered when you need it.
            </InfoText>
          </InfoFeature>
        </InfoFeatures>
      </Container>
    </InfoSectionContainer>
  );
};

export default InfoSection;
