import styled from "styled-components";

export const Container = styled.div`
  max-width: 80%;
  margin: 0 auto;

  @media screen and (min-width: 2000px) {
    max-width: 50%;
  }

  @media screen and (max-width: 870px) {
    max-width: 90%;
  }
`;
