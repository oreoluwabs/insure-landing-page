import React from "react";
import { ButtonLink } from "../Button";
import { HeroHeader } from "../Home/Hero/HeroElements";
import { Container } from "../Layout";
import { BannerContainer } from "./BannerElements";

const BannerComponent = () => {
  return (
    <Container>
      <BannerContainer>
        <HeroHeader>Find out more about how we work</HeroHeader>
        <ButtonLink to="">How we work</ButtonLink>
      </BannerContainer>
    </Container>
  );
};

export default BannerComponent;
