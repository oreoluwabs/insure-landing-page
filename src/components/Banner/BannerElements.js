import styled from "styled-components";

const mainColor = ({ theme }) => theme.colors.primary.main;

const getImage = (imageName) =>
  require(`../../assets/images/${imageName}`).default;

export const BannerContainer = styled.div`
  background-image: url(${getImage("bg-pattern-how-we-work-desktop.svg")});
  background-color: ${mainColor};
  background-repeat: no-repeat;
  background-position: top right;
  color: #fff;
  padding: 50px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  font-size: 14px;
  margin: 100px 0;

  h1 {
    width: 40%;
  }

  @media screen and (max-width: 870px) {
    background-image: url(${getImage("bg-pattern-how-we-work-mobile.svg")});
    // background-position: top right;

    text-align: center;
    flex-direction: column;
    justify-content: flex-start;

    h1 {
      margin: 50px;
      width: 100%;
    }
  }
`;
