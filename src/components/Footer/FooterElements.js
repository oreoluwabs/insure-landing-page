import styled from "styled-components";
import { Link as LinkRRouter } from "react-router-dom";

const veryDarkViolet = ({ theme }) => theme.colors.neutral["very-dark-violet"];
const darkGrayishViolet = ({ theme }) =>
  theme.colors.neutral["dark-grayish-violet"];
const veryLightGray = ({ theme }) => theme.colors.neutral["very-light-gray"];

const getImage = (imageName) =>
  require(`../../assets/images/${imageName}`).default;

export const FooterContainer = styled.footer`
  background-color: ${veryLightGray};
  background-image: url(${getImage("bg-pattern-footer-desktop.svg")});
  background-repeat: no-repeat;
  background-position: top 15% left;

  padding: 60px 0 0;

  @media screen and (max-width: 870px) {
    background-image: url(${getImage("bg-pattern-footer-mobile.svg")});
    background-position: top left;
  }
`;

export const FooterTop = styled.div`
  display: flex;
  border: none;
  border-bottom: 1px solid ${darkGrayishViolet};
  justify-content: space-between;
  align-items: center;
  padding-bottom: 20px;
  margin-bottom: 40px;

  @media screen and (max-width: 870px) {
    flex-direction: column;
  }
`;

export const SocialIcons = styled.div`
  display: flex;
  align-items: center;
`;

export const SocialLink = styled(LinkRRouter)`
  img {
    height: 20px;

    &:hover {
      filter: invert(1) sepia(10) saturate(6) hue-rotate(205deg);
      // fill: red;
    }
  }

  &:not(:last-of-type) {
    margin-right: 15px;
  }
`;

export const FooterLogo = styled(LinkRRouter)`
  @media screen and (max-width: 870px) {
    margin-bottom: 20px;
  }
`;

export const FooterBottom = styled.div`
  margin: 40px 0;
  display: flex;
  justify-content: space-between;

  @media screen and (max-width: 870px) {
    flex-direction: column;
  }
`;

export const FooterGroup = styled.div`
  font-size: 12px;
  text-transform: uppercase;
  font-weight: 700;
  letter-spacing: 2px;
  width: 100%;

  @media screen and (max-width: 870px) {
    margin-bottom: 10px;
    &:not(:first-of-type) {
      margin: 10px 0;
    }

    text-align: center;
  }
`;

export const FooterHeading = styled.p`
  color: ${darkGrayishViolet};
  margin-bottom: 20px;
`;

export const FooterItem = styled.div`
  margin: 5px 0;

  &:not(:last-of-type) {
    margin-bottom: 0;
  }
`;

export const FooterLink = styled(LinkRRouter)`
  color: ${veryDarkViolet};
  text-decoration: none;

  &:hover {
    text-decoration: underline;
  }
`;
