import React from "react";
import logo from "../../assets/images/logo.svg";
import FaceBookIcon from "../../assets/images/icon-facebook.svg";
import TwitterIcon from "../../assets/images/icon-twitter.svg";
import PinterestIcon from "../../assets/images/icon-pinterest.svg";
import InstagramIcon from "../../assets/images/icon-instagram.svg";
import { Container } from "../Layout";
import {
  FooterBottom,
  FooterContainer,
  FooterHeading,
  FooterGroup,
  FooterItem,
  FooterLink,
  FooterLogo,
  FooterTop,
  SocialIcons,
  SocialLink,
} from "./FooterElements";

const Footer = () => {
  return (
    <>
      <FooterContainer>
        <Container>
          <div>
            <FooterTop>
              <FooterLogo to="/">
                <img src={logo} alt="insure logo" />
              </FooterLogo>
              <SocialIcons>
                <SocialLink to="/">
                  <img src={FaceBookIcon} alt="facebook account" />
                </SocialLink>
                <SocialLink to="/">
                  <img src={TwitterIcon} alt="facebook account" />
                </SocialLink>
                <SocialLink to="/">
                  <img src={PinterestIcon} alt="facebook account" />
                </SocialLink>
                <SocialLink to="/">
                  <img src={InstagramIcon} alt="facebook account" />
                </SocialLink>
              </SocialIcons>
            </FooterTop>
            <FooterBottom>
              <FooterGroup>
                <FooterHeading>Our company</FooterHeading>
                <FooterItem>
                  <FooterLink to="/">How we work</FooterLink>
                </FooterItem>
                <FooterItem>
                  <FooterLink to="/">Why insure</FooterLink>
                </FooterItem>
                <FooterItem>
                  <FooterLink to="/">View plans</FooterLink>
                </FooterItem>
                <FooterItem>
                  <FooterLink to="/">Reviews</FooterLink>
                </FooterItem>
              </FooterGroup>

              <FooterGroup>
                <FooterHeading>Help me</FooterHeading>
                <FooterItem>
                  <FooterLink to="/">FAQ</FooterLink>
                </FooterItem>
                <FooterItem>
                  <FooterLink to="/">Terms of use</FooterLink>
                </FooterItem>
                <FooterItem>
                  <FooterLink to="/">Privacy policy</FooterLink>
                </FooterItem>
                <FooterItem>
                  <FooterLink to="/">Cookies</FooterLink>
                </FooterItem>
              </FooterGroup>

              <FooterGroup>
                <FooterHeading>Contact</FooterHeading>
                <FooterItem>
                  <FooterLink to="/">Sales</FooterLink>
                </FooterItem>
                <FooterItem>
                  <FooterLink to="/">Support</FooterLink>
                </FooterItem>
                <FooterItem>
                  <FooterLink to="/">Live chat</FooterLink>
                </FooterItem>
              </FooterGroup>

              <FooterGroup>
                <FooterHeading>Others</FooterHeading>
                <FooterItem>
                  <FooterLink to="/">Careers</FooterLink>
                </FooterItem>
                <FooterItem>
                  <FooterLink to="/">Press</FooterLink>
                </FooterItem>
                <FooterItem>
                  <FooterLink to="/">Licenses</FooterLink>
                </FooterItem>
              </FooterGroup>
            </FooterBottom>
          </div>
        </Container>
        <div className="attribution">
          Challenge by{" "}
          <a
            href="https://www.frontendmentor.io?ref=challenge"
            target="_blank"
            rel="noreferrer"
          >
            Frontend Mentor
          </a>
          . Coded by{" "}
          <a
            href="https://github.com/oreoluwa-bs"
            target="_blank"
            rel="noreferrer"
          >
            Oreoluwa Bimbo-Salami
          </a>
          .
        </div>
      </FooterContainer>
    </>
  );
};

export default Footer;
