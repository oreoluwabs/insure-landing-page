import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
  *, *::before,*::after {
    margin: 0px;
    padding: 0px;
  }

  body {
    font-family: ${(props) =>
      props.theme.typography.body["font-family"] || "san-serif"};
    letter-spacing: 1.2px;
    line-height: 1.5;
    color: ${(props) =>
      props.theme.colors.neutral["very-dark-violet"] || "black"};
  }

  h1,h2,h3,h4,h5,h6 {
    font-family: ${(props) =>
      props.theme.typography.headings["font-family"] || "serif"};
      line-height: 1.1;
  }
`;
